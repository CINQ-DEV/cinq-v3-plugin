<?php
// Google anylitics code in header
add_action('wp_head', 'add_to_header');
function add_to_header()
{
$GA_TRACKING_ID = get_field('google_analytics_code', 'option'); ?>
<!-- FAVICONS -->
<?php if (get_field('favicon', 'option')): ?>
  <!-- Standard Favicon -->
    <link rel="icon" type="image/x-icon" href="<?php the_field('favicon', 'option'); ?>" />
<?php endif; ?>
<?php if (get_field('apple_touch_114', 'option')): ?>
  <!-- For iPhone 4 Retina display: -->
      <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php the_field('apple_touch_114', 'option'); ?>">
<?php endif; ?>
<?php if (get_field('apple_touch_72', 'option')): ?>
  <!-- For iPad: -->
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php the_field('apple_touch_72', 'option'); ?>">
<?php endif; ?>
<?php if (get_field('apple_touch_57', 'option')): ?>
  <!-- For iPhone: -->
    <link rel="apple-touch-icon-precomposed" href="<?php the_field('apple_touch_57', 'option'); ?>">
<?php endif; ?>
<?php if (get_field('google_analytics_code', 'option')): ?>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $GA_TRACKING_ID ?>"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', '<?php echo $GA_TRACKING_ID ?>');
</script>
<?php endif; 
};
?>
